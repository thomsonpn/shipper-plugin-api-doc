# Shipper Plugin API Documentation
Welcome to Shipper Plugin API Documentation. This documentation is purposed to help developers retrieve Shipper data and post valid data. 

Until now we are still in development phase. If you found any uncommon response please tell us so we can fix it as soon as possible.

**General Information**
- Current Version: 0.3.1
- Last modified: 30 Januari 2018

**Version History**

| Version | Date | Information |
| ------ | ------ | ------ |
| 0.3.1 | 30 Jan, 2018 | For authentication, there will be no more Tokenize() and RefreshToken() endpoints to authenticate the request. Therefore for each request only need to add `api_key` header parameter instead of `grant_type` and `access_token`. |
| 0.2.15 | 26 Jan, 2018 | Major changes on header request formats and business logics for all endpoints. |
| 0.1.13 | 20 Dec, 2017 | First development completed. |

## Contents
 - [Error Response Example](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#error-response-example)
 - [Authentication](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#authentication)
 - [Search Domestic Rate](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#search-domestic-rate)
 - [Search International Rate](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#search-international-rate)
 - [Create Domestic Order](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#create-domestic-order)
 - [Create International Order](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#create-international-order)
 - [Search Area](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#search-area)
 - [Get Order Detail](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#get-order-detail)
 - [Get Order List](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#get-order-list)
 - [Cancel Order](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#cancel-order)
 - [Confirm Order](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#confirm-order)
 - [Get Provinces](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#get-provinces)
 - [Get Cities](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#get-cities)
 - [Get Suburbs](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#get-suburbs)
 - [Get Areas](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#get-areas)
 - [Get Domestic Rates](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#get-domestic-rates)
 - [Get International Rates](https://gitlab.com/thomsonpn/shipper-plugin-api-doc#get-international-rates)
 
## System Environment Variables
There are 2 variables for system environment. Please remember to set the variable to **Staging** or **Development** if you use the system for development purposes. For authention, 

| Variables | URL |
| ------ | ------ |
| Production | Coming soon |
| Sandbox | Coming soon |
| Staging | https://shipper.id/plugins/api/v1/ |


## Error Response Example

All error response field format are **similar**. The differences are the contents of each field values. Below are error response examples that will be received if something went wrong.

**Bad Request Example**

- Code: 400
- Content:

```javascript
{
    "status": "DATA_VALIDATION_FAILED",
    "message": "Some data are invalid.",
    "version": "1.0",
    "errors": [
        {
            "message": "Some fields are required.",
            "fields": [
                "q"
            ]
        }
    ]
}
```

**Unauthorized Request Example**
- Code: 401
- Content:

```javascript
{
    "status": "ACCESS_TOKEN_MISMATCH",
    "message": "No user found with access token given.",
    "version": "1.0"
}
```

## Authentication

Since this current version, all API requests only need to add `Api-Key` variable as its header parameter to make successful request. There is no need to tokenize the API Key to get the `access_token` value. Also for each request, there is no need to authenticate by `grant_type` and `access_token` to retrieve successful response data.


## Search Domestic Rate

Search shipping rate for domestic shipments.

**Endpont URL**

| Method | URL |
| ------ | ------ |
| GET | https://shipper.id/plugin/api/v1/rates/domestics/search |

**Query String Fields**

- Content-type: json/application

| Name | Required | Default | Type | Description |
| ------ | ------ | ------ | ------ | ------ |
| o | Y |  | int | Origin Area Id |
| d | Y |  | int | Destination Area Id |
| wt | N | 1 | int | Item Weight |
| v | N | 50000 | int | Item Value |
| l | N | 20 | int | Item Length |
| w | N | 10 | int | Item Width |
| h | N | 30 | int | Item Height |


**Success Response**
 - Code: 200
 - Content: 

 ```javascript
{
    "status": "OK",
    "message": "Successfully retrieving data.",
    "version": "1.0",
    "items": {
        "weight": 1,
        "volumeWeight": 1,
        "originArea": "Jelambar, Grogol Petamburan, Jakarta Barat",
        "destinationArea": "Malingdam, Eipumek, Pegunungan Bintang",
        "rule": "Text about Shipping Restriction is here",
        "rates": {
            "logistic": {
                "regular": [
                    {
                        "stop_origin": 11460,
                        "stop_destination": 99577,
                        "rate_id": 49,
                        "show_id": 1,
                        "min_day": 5,
                        "max_day": 10,
                        "rate_name": "Paket Kilat Khusus",
                        "description": "",
                        "logo_url": "http://cdn.shipper.cloud/logistic/small/POS.png",
                        "name": "POS Indonesia",
                        "liability": 10000,
                        "finalRate": 93000,
                        "insuranceRate": 550,
                        "compulsory_insurance": 0
                    },
                    {
                        "stop_origin": 51,
                        "stop_destination": 536,
                        "rate_id": 1,
                        "show_id": 1,
                        "min_day": 4,
                        "max_day": 6,
                        "rate_name": "REG",
                        "description": "",
                        "logo_url": "http://cdn.shipper.cloud/logistic/small/JNE.png",
                        "name": "JNE",
                        "liability": 10000,
                        "finalRate": 114000,
                        "insuranceRate": 5020,
                        "compulsory_insurance": 0
                    },
                    {
                        "stop_origin": 51,
                        "stop_destination": 536,
                        "rate_id": 58,
                        "show_id": 1,
                        "min_day": 4,
                        "max_day": 6,
                        "rate_name": "REG",
                        "description": "",
                        "logo_url": "http://cdn.shipper.cloud/logistic/small/SCP.png",
                        "name": "SiCepat",
                        "liability": 10000,
                        "finalRate": 114000,
                        "insuranceRate": 5020,
                        "compulsory_insurance": 0
                    },
                    {
                        "stop_origin": 51,
                        "stop_destination": 536,
                        "rate_id": 269,
                        "show_id": 1,
                        "min_day": 4,
                        "max_day": 6,
                        "rate_name": "REGULAR",
                        "description": "Regular Rate",
                        "logo_url": "http://cdn.shipper.cloud/logistic/small/ACM.png",
                        "name": "aCommerce",
                        "liability": 10000,
                        "finalRate": 114000,
                        "insuranceRate": 30,
                        "compulsory_insurance": 0
                    },
                    {
                        "stop_origin": 51,
                        "stop_destination": 536,
                        "rate_id": 290,
                        "show_id": 1,
                        "min_day": 4,
                        "max_day": 6,
                        "rate_name": "Priority",
                        "description": "Priority (Extra Packaging)",
                        "logo_url": "http://cdn.shipper.cloud/logistic/small/SCP.png",
                        "name": "SiCepat",
                        "liability": 10000,
                        "finalRate": 116000,
                        "insuranceRate": 5020,
                        "compulsory_insurance": 0
                    },
                    {
                        "stop_origin": 51,
                        "stop_destination": 8573,
                        "rate_id": 57,
                        "show_id": 1,
                        "min_day": 1,
                        "max_day": 3,
                        "rate_name": "Express",
                        "description": "",
                        "logo_url": "http://cdn.shipper.cloud/logistic/small/JNT.png",
                        "name": "J&T",
                        "liability": 10000,
                        "finalRate": 143000,
                        "insuranceRate": 5025,
                        "compulsory_insurance": 0
                    },
                    {
                        "stop_origin": 51,
                        "stop_destination": 536,
                        "rate_id": 7,
                        "show_id": 1,
                        "min_day": 12,
                        "max_day": 12,
                        "rate_name": "REP",
                        "description": "",
                        "logo_url": "http://cdn.shipper.cloud/logistic/small/RPX.png",
                        "name": "RPX",
                        "liability": 10000,
                        "finalRate": 149000,
                        "insuranceRate": 50,
                        "compulsory_insurance": 0
                    },
                    {
                        "stop_origin": 51,
                        "stop_destination": 536,
                        "rate_id": 8,
                        "show_id": 1,
                        "min_day": 12,
                        "max_day": 12,
                        "rate_name": "RGP",
                        "description": "",
                        "logo_url": "http://cdn.shipper.cloud/logistic/small/RPX.png",
                        "name": "RPX",
                        "liability": 10000,
                        "finalRate": 173000,
                        "insuranceRate": 50,
                        "compulsory_insurance": 0
                    },
                    {
                        "stop_origin": 51,
                        "stop_destination": 8573,
                        "rate_id": 15,
                        "show_id": 1,
                        "min_day": 10,
                        "max_day": 10,
                        "rate_name": "Regular",
                        "description": "",
                        "logo_url": "http://cdn.shipper.cloud/logistic/small/WHN.png",
                        "name": "Wahana",
                        "liability": 10000,
                        "finalRate": 399000,
                        "insuranceRate": 50,
                        "compulsory_insurance": 0
                    }
                ],
                "express": []
            }
        },
        "statusCode": 200
    }
}
``` 

## Search International Rate

Search shipping rate for international shipments.

**Endpont URL**

| Method | URL |
| ------ | ------ |
| GET | https://shipper.id/plugin/api/v1/rates/intls/search |

**Query String Fields**

- Content-type: json/application

| Name | Required | Default | Type | Description |
| ------ | ------ | ------ | ------ | ------ |
| o | Y |  | int | **ORIGIN COUNTRY ID** |
| d | Y |  | int | Destination Area Id |
| wt | N | 1 | int | Item Weight |
| v | N | 50000 | int | Item Value |
| l | N | 20 | int | Item Length |
| w | N | 10 | int | Item Width |
| h | N | 30 | int | Item Height |


**Success Response**
 - Code: 200
 - Content: 

 ```javascript
{
    "status": "success",
    "data": {
        "title": "OK",
        "content": "Successfully retrieving rates",
        "weight": 1,
        "volumeWeight": 0.2,
        "destinationCountry": "AFGHANISTAN",
        "rule": "Text about Shipping Restriction is here",
        "rates": {
            "logistic": {
                "international": [
                    {
                        "stop_origin": 21099,
                        "stop_destination": 20834,
                        "name": "POS Indonesia",
                        "rate_id": 210,
                        "rate_name": "EMS",
                        "finalRate": 886275,
                        "min_day": 4,
                        "max_day": 5,
                        "logo_url": "http://cdn.shipper.cloud/logistic/small/POS.png",
                        "liability": 10000,
                        "insuranceRate": 5000,
                        "compulsory_insurance": 0
                    }
                ]
            }
        },
        "statusCode": 200
    }
}
``` 


## Create Domestic Order

Create Shipper Domestic order.

**Endpont URL**

| Method | URL | Need Auth |
| ------ | ------ | ------ |
| POST | https://shipper.id/plugin/api/v1/orders/domestics | Y |

**Request Body Fields**

- Content-type: json/application

| Name | Required | Default | Type | Description |
| ------ | ------ | ------ | ------ | ------ |
| o | Y |  | int |  |
| o_address | Y |  | string |  |
| o_addr_direction | N | Empty string | string |  |
| d | Y |  | string |  |
| d_address | Y |  | string |  |
| d_addr_direction | N | Empty string | int |  |
| csgnee_name | Y |  | string |  |
| csgnee_phone | Y |  | int | Minimum character length is 6. |
| pck_type | Y |  | int | Only accept values of 1 (for document), 2 (for small) or 3 (for medium). |
| wt | Y |  | int | Maximum value is 10. |
| w | Y |  | int |  |
| l | Y |  | int |  |
| h | Y |  | int |  |
| content | Y |  | string |  |
| external_code | N | Empty string | string | Referal code |
| item_price | Y |  | double |  |
| payment_type | Y |  | string |  |
| use_ins | Y |  | bool |  |
| is_cod | Y |  | bool |  |
| logistic | Y |  | int | Only accept values of 1 (for regular) and 2 (for express). |
| rate_code | Y |  | int |  |


**Request Body Example**

 ```javascript
{
	"o":4803,
	"o_address":"Jln Bhakti",
	"o_addr_direction":"",
	"d":42547,
	"d_address":"Jln Pahlawan",
	"d_addr_direction":"",
	"csgnee_name":"Syamsul",
	"csgnee_phone":"089512321322",
	"pck_type":2,
	"wt":1,
	"l":10,
	"w":10,
	"h":10,
	"content":"Buku",
	"external_code":"",
	"item_price":10000,
	"payment_type":"postpay",
	"use_ins":1,
	"is_cod": 0,
	"logistic":1,
	"rate_code":1
}
``` 

**Success Response**
 - Code: 201
 - Content: 

 ```javascript
{
    "status": "CREATED",
    "message": "Successfully created data.",
    "version": "1.0",
    "items": {
        "created_code": "5a55fdb2de34452276318018"
    }
}
```


## Create International Order

Create Shipper International order.

**Endpont URL**

| Method | URL | Need Auth |
| ------ | ------ | ------ |
| POST | https://shipper.id/plugin/api/v1/orders/intls | Y |

**Request Body Fields**

- Content-type: json/application

| Name | Required | Default | Type | Description |
| ------ | ------ | ------ | ------ | ------ |
| o | Y |  | integer |  |
| o_address | Y |  | string |  |
| o_addr_direction | N | Empty string | string |  |
| d | Y |  | string |  |
| d_address | Y |  | string |  |
| d_addr_direction | N | Empty string | int |  |
| d_area | Y |  | int |  |
| d_suburb | Y |  | int |  |
| d_city | Y |  | int |  |
| d_province | Y |  | int |  |
| d_postcode | Y |  | int |  |
| csgnee_name | Y |  | string |  |
| csgnee_phone | Y |  | int | Minimum character length is 6. |
| pck_type | Y |  | string | Only accept values of 1 (for document), 2 (for small) or 3 (for medium). |
| wt | Y |  | int | Maximum value is 10. |
| w | Y |  | int |  |
| l | Y |  | int |  |
| h | Y |  | int |  |
| content | Y |  | string |  |
| external_code | N | Empty string | string | Referal code |
| item_price | Y |  | double |  |
| payment_type | Y |  | string |  |
| use_ins | Y |  | bool |  |
| is_cod | Y |  | bool |  |
| logistic | Y |  | int | Only accept values of 1 (for regular) and 2 (for express). |
| rate_code | Y |  | int |  |


**Request Body Example**

 ```javascript
{
	"o":4803,
	"o_address":"Jln Bhakti",
	"o_addr_direction":"",
	"d":1,
	"d_address":"Jln Pahlawan",
	"d_addr_direction":"",
	"d_area":"Sei Bamban",
	"d_suburb":"Belutu",
	"d_city":"South California",
	"d_province":"California",
	"d_postcode":"22121",
	"csgnee_name":"Syamsul",
	"csgnee_phone":"089512321322",
	"pck_type":2,
	"wt":1,
	"l":10,
	"w":10,
	"h":10,
	"content":"Buku",
	"external_code":"",
	"item_price":10000,
	"payment_type":"postpay",
	"use_ins":1,
	"is_cod": 0,
	"logistic":1,
	"rate_code":210
}
``` 

**Success Response**
 - Code: 201
 - Content: 

 ```javascript
{
    "status": "CREATED",
    "message": "Successfully created data.",
    "version": "1.0",
    "items": {
        "created_code": "5a55fdb2de34452276318018"
    }
}
``` 

## Search Area

Search an area to get the area code.

**Endpont URL**

| Method | URL | Need Auth |
| ------ | ------ | ------ |
| GET | https://shipper.id/plugin/api/v1/geodata/search?k=Mertoyudan | Y |

**Query String Fields**

| Name | Required | Default | Type | Description |
| ------ | ------ | ------ | ------ | ------ |
| k | Y |  | string | Area keyword |

**Success Response Example**
 - Code: 200
 - Content: 

 ```javascript
{
    "status": "OK",
    "message": "Successfully retrieving data.",
    "version": "1.0",
    "items": [
        {
            "label": "Mertoyudan, Magelang, Kab.",
            "value": "1633|95|10",
            "city_name": "Magelang, Kab.",
            "suburb_name": "Mertoyudan",
            "area_name": "",
            "order_list": 2
        },
        {
            "label": "Mertoyudan, Mertoyudan, Magelang, Kab., 56172",
            "value": "56172|17306|1633|95|10",
            "city_name": "Magelang, Kab.",
            "suburb_name": "Mertoyudan",
            "area_name": "Mertoyudan",
            "order_list": 3
        }
    ]
}
``` 


## Get Order Detail

Get shipper order detail.

**Endpont URL**

| Method | URL | Need Auth |
| ------ | ------ | ------ |
| GET | https://shipper.id/plugin/api/v1/me/orders/{code} | Y |

**URL Params**

| Name | Required | Default | Type | Descroption |
| ------ | ------ | ------ | ------ | ------ |
| code | Y |  | int | Shipper order code (e.g: PL13) |

**Success Response Example**
 - Code: 200
 - Content: 

 ```javascript
{
    "status": "OK",
    "message": "Successfully retrieving data.",
    "version": "1.0",
    "items": {
        "tracking": [],
        "detail": {
            "id": "1A02488",
            "externalID": "",
            "labelChecksum": "20a5ef6922cf72faa28ab3a871880e52",
            "consigner": {
                "id": "5a6577213d25d062508b4567",
                "name": "Plugin API Sandbox Testing Account",
                "phoneNumber": "+62821000000001"
            },
            "consignee": {
                "id": "5a54a6440abed61eab8c5142",
                "name": "Syamsul",
                "phoneNumber": "089512321322"
            },
            "awbNumber": "",
            "package": {
                "itemName": "Buku",
                "contents": "Buku",
                "price": {
                    "value": 10000,
                    "UoM": "IDR"
                },
                "dimension": {
                    "length": {
                        "value": 10,
                        "UoM": "cm"
                    },
                    "height": {
                        "value": 10,
                        "UoM": "cm"
                    },
                    "width": {
                        "value": 10,
                        "UoM": "cm"
                    }
                },
                "weight": {
                    "value": 1,
                    "UoM": "kg"
                },
                "type": "Paket Kecil",
                "pictureURL": "",
                "isConfirmed": 0,
                "volumeWeight": {
                    "value": 1,
                    "UoM": "kg"
                }
            },
            "origin": {
                "stopID": 51,
                "address": "Jln Bhakti",
                "direction": "",
                "areaID": 4803,
                "areaName": "Grogol",
                "suburbID": 497,
                "suburbName": "Grogol Petamburan",
                "cityID": 42,
                "cityName": "Jakarta Barat",
                "provinceID": 6,
                "provinceName": "DKI Jakarta",
                "countryID": 228,
                "countryName": "INDONESIA",
                "postcode": "11450"
            },
            "destination": {
                "stopID": 7566,
                "address": "",
                "direction": "",
                "areaID": 42547,
                "areaName": "Panglima Saman",
                "suburbID": 3721,
                "suburbName": "Rundeng",
                "cityID": 240,
                "cityName": "Subulussalam",
                "provinceID": 21,
                "provinceName": "Aceh",
                "countryID": 228,
                "countryName": "INDONESIA",
                "postcode": "24884"
            },
            "driver": {
                "name": "",
                "phoneNumber": "",
                "vehicleType": "",
                "vehicleNumber": "",
                "agentID": 0,
                "agentName": ""
            },
            "courier": {
                "rate_id": 1,
                "rate_name": "REG",
                "name": "JNE",
                "shipmentType": "Regular",
                "min_day": 2,
                "max_day": 3,
                "rate": {
                    "value": 56020,
                    "UoM": "IDR"
                }
            },
            "activeDate": "",
            "creationDate": "2018-01-22T08:17:57+00:00",
            "lastUpdatedDate": "2018-01-22T08:17:57+00:00",
            "createdBy": 0,
            "useInsurance": 1,
            "shipmentArea": "domestic",
            "cod": {
                "order": 0
            }
        }
    }
}
``` 

## Get Order List

Get its own order list.

**Endpont URL**

| Method | URL | Need Auth |
| ------ | ------ | ------ |
| GET | https://shipper.id/plugin/api/v1/me/orders | Y |


**Success Response Example**
 - Code: 200
 - Content: 

 ```javascript
{
    "status": "OK",
    "message": "Successfully retrieving data.",
    "version": "1.0",
    "items": [
        {
            "id": "1A00020",
            "creationDate": "2017-02-28T08:29:55+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00019",
            "creationDate": "2017-02-28T08:17:41+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00018",
            "creationDate": "2017-02-28T02:01:11+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00017",
            "creationDate": "2017-02-23T03:02:10+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00016",
            "creationDate": "2017-02-21T03:58:46+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00015",
            "creationDate": "2017-02-20T15:49:44+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00014",
            "creationDate": "2017-02-20T15:46:36+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00013",
            "creationDate": "2017-02-20T15:26:42+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00012",
            "creationDate": "2017-02-20T10:33:29+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00011",
            "creationDate": "2017-02-20T03:04:57+00:00",
            "lastStatus": "Diterima Partner"
        },
        {
            "id": "1A00010",
            "creationDate": "2017-02-20T02:49:55+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00009",
            "creationDate": "2017-02-19T22:08:15+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00008",
            "creationDate": "2017-02-19T22:07:23+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00007",
            "creationDate": "2017-02-19T22:05:19+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00006",
            "creationDate": "2017-02-19T11:23:57+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00005",
            "creationDate": "2017-02-19T09:48:23+00:00",
            "lastStatus": "Order Diterima"
        },
        {
            "id": "1A00003",
            "creationDate": "2017-02-19T08:27:44+00:00",
            "lastStatus": "Order Dibatalkan"
        },
        {
            "id": "1A00004",
            "creationDate": "2017-02-16T13:06:56+00:00",
            "lastStatus": "Berhasil Dikirim"
        },
        {
            "id": "1A00002",
            "creationDate": "2017-02-09T15:30:58+00:00",
            "lastStatus": "Dijemput Driver"
        },
        {
            "id": "1A00001",
            "creationDate": "2017-02-07T14:50:49+00:00",
            "lastStatus": "Order Dibatalkan"
        }
    ]
}
``` 

## Cancel Order

Cancel specified order.

**Endpont URL**

| Method | URL | Need Auth |
| ------ | ------ | ------ |
| PUT | plugin/api/v1/orders/cancel/{code} | Y |
 

**Success Response Example**
 - Code: 200
 - Content: 

**URL Params**

| Name | Required | Default | Type | Descroption |
| ------ | ------ | ------ | ------ | ------ |
| code | Y |  | string | Shipper order code (e.g: PLG1) |

**Request Body Fields**

| Name | Required | Default | Type | Descroption |
| ------ | ------ | ------ | ------ | ------ |
| ref_code | N |  | string | Reference code |

```javascript
{
    "status": "CANCELLED",
    "message": "The order data with id of PLG1 is successfully cancelled.",
    "version": "1.0"
}
``` 

## Confirm Order

Confirm and activate specified order.

**Endpont URL**

| Method | URL | Need Auth |
| ------ | ------ | ------ |
| PUT | plugin/api/v1/orders/confirm/{code} | Y |

**URL Params**

| Name | Required | Default | Type | Descroption |
| ------ | ------ | ------ | ------ | ------ |
| code | Y |  | string | Shipper order code (e.g: PLG1) |

**Success Response Example**
 - Code: 200
 - Content: 

 ```javascript
{
    "status": "CONFIRMED",
    "message": "The order data with id of PLG1 is successfully confirmed.",
    "version": "1.0"
}
``` 

## Get Provinces

Get domestic province data. No query strings available.

**Endpont URL**

| Method | URL |
| ------ | ------ |
| GET | plugin/api/v1/geodata/provs |

**Success Response Example**
 - Code: 200
 - Content: 

 ```javascript
{
    "status": "OK",
    "message": "Successfully retreived data.",
    "version": "1.0",
    "items": [
        {
            "label": "Bali",
            "value": "1"
        },
        {
            "label": "Bangka Belitung",
            "value": "2"
        },
        {
            "label": "Banten",
            "value": "3"
        },
        {
            "label": "Bengkulu",
            "value": "4"
        },
        {
            "label": "DI Yogyakarta",
            "value": "5"
        },
        {
            "label": "DKI Jakarta",
            "value": "6"
        },
        {
            "label": "Gorontalo",
            "value": "7"
        },
        {
            "label": "Jambi",
            "value": "8"
        },
        {
            "label": "Jawa Barat",
            "value": "9"
        },
        {
            "label": "Jawa Tengah",
            "value": "10"
        },
        {
            "label": "Jawa Timur",
            "value": "11"
        },
        {
            "label": "Kalimantan Barat",
            "value": "12"
        },
        {
            "label": "Kalimantan Selatan",
            "value": "13"
        },
        {
            "label": "Kalimantan Tengah",
            "value": "14"
        },
        {
            "label": "Kalimantan Timur",
            "value": "15"
        },
        {
            "label": "Kalimantan Utara",
            "value": "16"
        },
        {
            "label": "Kepulauan Riau",
            "value": "17"
        },
        {
            "label": "Lampung",
            "value": "18"
        },
        {
            "label": "Maluku Utara",
            "value": "19"
        },
        {
            "label": "Maluku",
            "value": "20"
        },
        {
            "label": "Aceh",
            "value": "21"
        },
        {
            "label": "Nusa Tenggara Barat",
            "value": "22"
        },
        {
            "label": "Nusa Tenggara Timur",
            "value": "23"
        },
        {
            "label": "Papua Barat",
            "value": "24"
        },
        {
            "label": "Papua",
            "value": "25"
        },
        {
            "label": "Riau",
            "value": "26"
        },
        {
            "label": "Sulawesi Barat",
            "value": "27"
        },
        {
            "label": "Sulawesi Selatan",
            "value": "28"
        },
        {
            "label": "Sulawesi Tengah",
            "value": "29"
        },
        {
            "label": "Sulawesi Tenggara",
            "value": "30"
        },
        {
            "label": "Sulawesi Utara",
            "value": "31"
        },
        {
            "label": "Sumatera Barat",
            "value": "32"
        },
        {
            "label": "Sumatera Selatan",
            "value": "33"
        },
        {
            "label": "Sumatera Utara",
            "value": "34"
        }
    ]
}
``` 

## Get Cities

Get domestic city data.

**Endpont URL**

| Method | URL |
| ------ | ------ |
| GET | plugin/api/v1/geodata/cities |

**Query Strings**

| Name | Required | Default | Type | Descroption |
| ------ | ------ | ------ | ------ | ------ |
| prov | Y |  | int | Province code |

**Success Response Example**
 - Code: 200
 - Content: 

 ```javascript
{
    "status": "OK",
    "message": "Successfully retreived data.",
    "version": "1.0",
    "items": [
        {
            "label": "Mandailing Natal",
            "value": "443"
        },
        {
            "label": "Nias Selatan",
            "value": "444"
        },
        {
            "label": "Nias",
            "value": "445"
        },
        {
            "label": "Nias Barat",
            "value": "446"
        },
        {
            "label": "Gunungsitoli",
            "value": "447"
        },
        {
            "label": "Nias Utara",
            "value": "448"
        },
        {
            "label": "Padang Lawas",
            "value": "449"
        },
        {
            "label": "Tapanuli Selatan",
            "value": "450"
        },
        {
            "label": "Padang Lawas Utara",
            "value": "451"
        },
        {
            "label": "Padang Sidempuan",
            "value": "452"
        },
        {
            "label": "Tapanuli Tengah",
            "value": "453"
        },
        {
            "label": "Sibolga",
            "value": "454"
        },
        {
            "label": "Tapanuli Utara",
            "value": "455"
        },
        {
            "label": "Humbang Hasundutan",
            "value": "456"
        },
        {
            "label": "Samosir",
            "value": "457"
        },
        {
            "label": "Toba Samosir",
            "value": "458"
        },
        {
            "label": "Dairi",
            "value": "459"
        },
        {
            "label": "Pakpak Bharat",
            "value": "460"
        },
        {
            "label": "Karo",
            "value": "461"
        },
        {
            "label": "Labuhan Batu Utara",
            "value": "462"
        },
        {
            "label": "Labuhan Batu",
            "value": "463"
        },
        {
            "label": "Labuhan Batu Selatan",
            "value": "464"
        },
        {
            "label": "Asahan",
            "value": "465"
        },
        {
            "label": "Tanjung Balai",
            "value": "466"
        },
        {
            "label": "Batu Bara",
            "value": "467"
        },
        {
            "label": "Simalungun",
            "value": "468"
        },
        {
            "label": "Pematang Siantar",
            "value": "469"
        },
        {
            "label": "Serdang Bedagai",
            "value": "470"
        },
        {
            "label": "Langkat",
            "value": "471"
        },
        {
            "label": "Binjai",
            "value": "472"
        },
        {
            "label": "Tebing Tinggi",
            "value": "473"
        },
        {
            "label": "Deli Serdang",
            "value": "474"
        },
        {
            "label": "Medan",
            "value": "475"
        }
    ]
}
``` 

## Get Suburbs

Get domestic suburb data.

**Endpont URL**

| Method | URL |
| ------ | ------ |
| GET | plugin/api/v1/geodata/suburbs |

**Query Strings**

| Name | Required | Default | Type | Descroption |
| ------ | ------ | ------ | ------ | ------ |
| city | Y |  | int | City code |

**Success Response Example**
 - Code: 200
 - Content: 

 ```javascript
{
    "status": "OK",
    "message": "Successfully retreived data.",
    "version": "1.0",
    "items": [
        {
            "label": "Padang Hilir",
            "value": "6947"
        },
        {
            "label": "Tebing Tinggi Kota",
            "value": "6948"
        },
        {
            "label": "Padang Hulu",
            "value": "6949"
        },
        {
            "label": "Bajenis",
            "value": "6950"
        },
        {
            "label": "Rambutan",
            "value": "6951"
        }
    ]
}
``` 

## Get Areas

Get domestic area data following with its postal code. No query strings available.

**Endpont URL**

| Method | URL |
| ------ | ------ |
| GET | plugin/api/v1/geodata/areas |

**Query Strings**

| Name | Required | Default | Type | Descroption |
| ------ | ------ | ------ | ------ | ------ |
| sub | Y |  | int | Suburb code |

**Success Response Example**
 - Code: 200
 - Content: 

 ```javascript
{
    "status": "OK",
    "message": "Successfully retreived data.",
    "version": "1.0",
    "items": [
        {
            "label": "Tanjung Marulak Hilir",
            "value": "80667",
            "postcode": "20618"
        },
        {
            "label": "Tanjung Marulak",
            "value": "80668",
            "postcode": "20617"
        },
        {
            "label": "Mekar Sentosa",
            "value": "80670",
            "postcode": "20618"
        },
        {
            "label": "Sri Padang",
            "value": "80671",
            "postcode": "20616"
        },
        {
            "label": "Lalang",
            "value": "80672",
            "postcode": "20614"
        },
        {
            "label": "Rantau Laban",
            "value": "80673",
            "postcode": "20614"
        },
        {
            "label": "Karya Jaya",
            "value": "80680",
            "postcode": "20611"
        }
    ]
}
``` 

End of page.